# PHPUnit functionality extension
---
Allows to mark group of tests as skipped.
Resetting singleton instance functionality.

### Installing
---
Add following code to your "composer.json" file
```json
    "repositories": [
        {
            "type": "vcs",
            "url":  "https://bitbucket.org/donbidon/lib-phpunit"
        }
    ],
    "require": {
        "donbidon/lib-phpunit": "dev-master"
    }
```
and run `composer update`.

### Usage
---
#### Mark tests as skipped
```php
<?php

require_once "/path/to/vendor/autoload.php";

class MyTest extends \donbidon\Lib\PHPUnit\TestCase
{
    public function testFoo()
    {
        $expected = 1;
        $actual   = 2;
        if ($expected !== $actual) {
            $this->skipGroup('someGroup');
        }
        $this->assertEquals($expected, $actual);
    }

    public function testBar()
    {
        $this->checkGroupIfSkipped('someGroup');
        $this->assertEquals(1, 1);
    }
}
```
will produce following output:
```
FAILURES!
Tests: 2, Assertions: 1, Failures: 1, Skipped: 1.
```
#### Resetting singleton instance
```php
<?php

require_once "/path/to/vendor/autoload.php";

class Foo
{
    use \donbidon\Lib\PHPUnit\T_ResetInstance;

    protected static $myInstance;

    public static function getInstance()
    {
        if (!is_object(self::$myInstance)) {
            self::$myInstance = new self;
        }

        return self::$myInstance;
    }

    protected function __construct()
    {
        self::setInstancePropertyName('myInstance');
        $this->allowToResetInstance();

        echo sprintf(
            "%s::%s called\n",
            __CLASS__,
            __METHOD__
        );
    }
}

Foo::getInstance();
Foo::resetInstance();
Foo::getInstance();
```
will output:
```
Foo::__construct() called
Foo::__construct() called
```
