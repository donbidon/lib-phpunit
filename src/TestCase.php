<?php
/**
 * PHPUnit functionality extension.
 *
 * Allows to mark group of tests as skipped.<br />
 * Contains debug functionality.
 *
 * @copyright <a href="http://donbidon.rf.gd/" target="_blank">donbidon</a>
 * @license   https://opensource.org/licenses/mit-license.php
 */

namespace donbidon\Lib\PHPUnit;

error_reporting(E_ALL);

/**
 * PHPUnit functionality extension.
 *
 * Allows to mark group of tests as skipped.
 * ```php
 * <?php
 *
 * class MyTest extends \donbidon\Lib\PHPUnit\TestCase
 * {
 *     public function testFoo()
 *     {
 *         $expected = 1;
 *         $actual   = 2;
 *         if ($expected !== $actual) {
 *             // Mark group of tests as skipped
 *             $this->skipGroup('someGroup');
 *         }
 *         // Failed assertion
 *         $this->assertEquals($expected, $actual);
 *     }
 *
 *     public function testBar()
 *     {
 *         // Skip test
 *         $this->checkGroupIfSkipped('someGroup');
 *         // Following code won't run
 *         $this->assertEquals(1, 1);
 *     }
 * }
 * ```
 * will produce following output:
 * ```
 * FAILURES!
 * Tests: 2, Assertions: 1, Failures: 1, Skipped: 1.
 * ```
 */
class TestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * Groups of tests to skip
     *
     * @var array
     */
    private static $groupsToSkip = [];

    /**
     * Sets up group to skip.
     *
     * @param  string $group
     * @param  bool   $separateByClass  Flag specifying to separate groups by current class name
     * @return void
     */
    protected static function skipGroup($group, $separateByClass = TRUE)
    {
        self::modyfyGroup($group, $separateByClass);
        self::$groupsToSkip[$group] = TRUE;
    }

    /**
     * Marks test skipped if group of tests has to be skipped.
     *
     * @param  string $group
     * @param  bool   $separateByClass  Flag specifying to separate groups by current class name
     * @return void
     */
    protected static function checkGroupIfSkipped($group, $separateByClass = TRUE)
    {
        self::modyfyGroup($group, $separateByClass);
        if (isset(self::$groupsToSkip[(string)$group])) {
            self::markTestSkipped(sprintf(
                "[SKIPPED] Some previous tests from '%s' group are skipped or incompleted",
                $group
            ));
        }
    }

    /**
     * Skips test by OS.
     *
     * @param  string $regExp           Regexp for PHP_OS constant value
     * @param  string $message          Message
     * @param  string $group
     * @param  bool   $separateByClass
     * @return void
     * @link   http://php.net/manual/en/reserved.constants.php PHP_OS constant
     * @see    self::skipGroup() $group and $separateByClass args
     */
    protected function skipByOS($regExp, $message = "", $group = "", $separateByClass = TRUE)
    {
        if (preg_match($regExp, PHP_OS)) {
            self::markTestSkipped(sprintf(
                "[SKIPPED][OS][%s]%s",
                PHP_OS,
                "" === $message ? "" : sprintf(" %s", $message)
            ));
            if ("" !== $group) {
                self::skipGroup($group, $separateByClass);
            }
        }
    }

    /**
     * Outputs string.
     *
     * @param  string $string
     * @return void
     */
    protected function _e($string)
    {
        fwrite(STDERR, $string);
    }

    /**
     * Prints human-readable information about a variable.
     *
     * @param  mixed $expression
     * @return void
     * @link   http://php.net/manual/en/function.print-r.php
     */
    protected function _pr($expression)
    {
        $this->_e(print_r($expression, TRUE));
    }

    /**
     * Dumps information about a variable.
     *
     * @param  mixed $expression
     * @return void
     * @link   http://php.net/manual/en/function.var-dump.php
     */
    protected function _vd($expression)
    {
        ob_start();
        var_dump($expression);
        $string = ob_get_contents();
        ob_end_clean();
        $this->_e($string);
    }

    /**
     * Outputs a parsable string representation of a variable.
     *
     * @param  mixed $expression
     * @return void
     * @link   http://php.net/manual/en/function.var-export.php
     */
    protected function _ve($expression)
    {
        $this->_e(var_export($expression, TRUE));
    }

    /**
     * Modifies group name according to arguments.
     *
     * @param    &string $group
     * @param    bool    $separateByClass  Flag specifying to separate groups by current class name
     * @return   void
     * @internal
     */
    protected static function modyfyGroup(&$group, $separateByClass)
    {
        $group = (string)$group;
        if ($separateByClass) {
            $group = sprintf("%s::%s", __CLASS__, $group);
        }
    }
}
